class Perro:
    tipo = 'canino'
    trucos = []
    def __init__(self, nombre):
        self.nombre = nombre

    def agregar_truco(self, truco):
        self.trucos.append(truco)

    def __str__(self):
        return self.nombre + ' ' + ', '.join(self.trucos)
    